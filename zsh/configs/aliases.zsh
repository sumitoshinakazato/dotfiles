# file manipulation
alias ls="exa --group-directories-first --icons"
alias ll="exa --group-directories-first --icons -al"
alias rm="trash"
alias cp="cp -rvi"
alias tree="exa --group-directories-first --icons -T"

alias cat="bat -p"

alias vi="nvim"

alias gitssh="eval `ssh-agent` ssh-add ~/.ssh/codeberg/sumitoshinakazato"
