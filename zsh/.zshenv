ZDOTDIR=~/.config/zsh

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_STATE_HOME=${XDG_STATE_HOME:="$HOME/.local/state"}


# cargo
export CARGO_HOME="$XDG_DATA_HOME"/cargo

# less
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history

# rustup
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup

# xsession errors
export ERRFILE="$XDG_CACHE_HOME"/X11/xsession-errors

# gtkrc 2.0
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc

# go
export GOPATH="$XDG_CACHE_HOME"/go

# icons
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
