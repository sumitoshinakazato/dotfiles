#!/bin/bash

echo ┌─────────────────────────────────────────────────────────┐
echo "│                                                         │"

# Backup config directory
mv ~/.config ~/old_config_backup
mkdir ~/.config
echo "│ - backup config directory [Done]                        │"

# alacritty
ln -s ~/Documents/Dev/dotfiles/alacritty ~/.config/alacritty
echo "│ - alacritty config [Done]                               │"

# i3
ln -s ~/Documents/Dev/dotfiles/i3 ~/.config/i3
echo "│ - i3 config [Done]                                      │"

# polybar
ln -s ~/Documents/Dev/dotfiles/polybar ~/.config/polybar
echo "│ - polybar config [Done]                                 │"

# git
ln -s ~/Documents/Dev/dotfiles/git ~/.config/git
echo "│ - git config [Done]                                     │"

# zsh
mkdir -p ~/.config/zsh
mkdir -p ~/.cache/zsh
ln -s ~/Documents/Dev/dotfiles/zsh/configs ~/.config/zsh/configs
ln -s ~/Documents/Dev/dotfiles/zsh/plugins ~/.config/zsh/plugins
ln -s ~/Documents/Dev/dotfiles/zsh/.zshrc ~/.config/zsh/.zshrc
ln -s ~/Documents/Dev/dotfiles/zsh/.zshenv ~/.config/zsh/.zshenv
echo "│ - zsh config [Done]                                     │"

# nvim
git clone --quiet https://github.com/AstroNvim/AstroNvim ~/.config/nvim
nvim +PackerSync
echo "│ - nvim config [Done]                                    │"

# Rofi
ln -s ~/Documents/Dev/dotfiles/rofi ~/.config/rofi
echo "│ - rofi config [Done]                                    │"

echo "│                                                         │"
echo ├─────────────────────────────────────────────────────────┤
echo "│                                                         │"

# addictinal twiks
echo "│ edit /etc/zsh/zshenv                                    │"
echo "│                                                         │"
echo "│ export ZDOTDIR─\"\$HOME\"/.config/zsh                      │"

echo "│                                                         │"
echo ├─────────────────────────────────────────────────────────┤
echo "│                                                         │"

echo "│ edit /etc/X11/xorg.conf.d/30-touchpad.conf              │"
echo "│                                                         │"
echo "│ Section \"InputClass\"                                    │"
echo "│     Identifier \"touchpad\"                               │"
echo "│     Driver \"libinput\"                                   │"
echo "│     MatchIsTouchpad \"on\"                                │"
echo "│     Option \"Tapping\" \"on\"                               │"
echo "│     Option \"NaturalScrolling\" \"True\"                    │"
echo "│ EndSection                                              │"

echo "│                                                         │"
echo └─────────────────────────────────────────────────────────┘
